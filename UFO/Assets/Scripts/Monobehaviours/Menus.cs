using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Menus : MonoBehaviour
{
    public GameObject mainMenuCanvas;
    public GameObject pauseMenuCanvas;
    public GameManager gm;


    //bool variable to check if pause menu is on or off
    public bool isPaused;

    private void Start()
    {
        GameObject[] menus = GameObject.FindGameObjectsWithTag("AllCanvas");
        if (menus.Length > 1)
        {
            Destroy(this.gameObject);
        }
        pauseMenuCanvas.SetActive(false);
        isPaused = false;
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += HandleSceneLoad;
        mainMenuCanvas = this.transform.GetChild(0).gameObject;
        pauseMenuCanvas = this.transform.GetChild(2).gameObject;
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void HandleSceneLoad(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "MainMenu")
        {
            mainMenuCanvas.SetActive(true);
        }
        else
        {
            if(mainMenuCanvas != null)
            {
                mainMenuCanvas.SetActive(false);
            }
        }
    }

    //quits the game
    public void QuitGame()
    {
        //Closes the application
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex > 0)
        {
            if (isPaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void PauseGame()
    {
        pauseMenuCanvas.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void ResumeGame()
    {
        pauseMenuCanvas.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    //loads main menu
    public void BackToMainMenu()
    {
        gm.highScore = 0;
        SceneManager.LoadScene(0);
        GameObject.Find("AllCanvas").transform.GetChild(2).gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

}
