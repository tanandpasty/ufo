using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public bool musicOn;
    public bool sfxOn = true;
    public AudioSource audioSource;
    public float sfx = 1.0f;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        if (GameObject.FindGameObjectsWithTag("AudioManager").Length > 1)
        {
            Destroy(this.gameObject);
        }
        TurnMusicOff();
    }

    public void TurnMusicOn()
    {
        musicOn = true;
        audioSource.mute = false;
    }

    public void TurnMusicOff()
    {
        musicOn = false;
        audioSource.mute = true;
    }

    public void TurnSFXOn()
    {
        sfxOn = true;
        sfx = 1.0f;
    }

    public void TurnSFXOff()
    {
        sfxOn = false;
        sfx = 0.0f;
    }

}
