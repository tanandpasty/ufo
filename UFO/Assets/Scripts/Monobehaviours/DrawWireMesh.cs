using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawWireMesh : MonoBehaviour
{
    // public float radiusSize will be used to set how large the drawn sphere is
    public float radiusSize;

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, radiusSize);
    }
}
