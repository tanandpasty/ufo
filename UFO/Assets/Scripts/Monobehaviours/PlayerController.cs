using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // public Transform point1, point2, point3, and point4 will reference the four points which
    // define the bezier curve on which the frisbee travels.
    public Transform point1;
    public Transform point2;
    public Transform point3;
    public Transform point4;

    // public Transform points is the parent object of the above four points.
    public Transform points;

    // public Transform pivot is used to control the angle of the frisbee.
    public Transform pivot;

    // public IEnumerator throwFisbee is the Coroutine used to throw the frisbee.
    public IEnumerator throwFrisbee;

    // public float speed is the speed at which the player can move the points.
    public float speed = 2.0f;

    // public RigidBody rb is the frisbee's rigidbody
    public Rigidbody rb;

    public BoxCollider bc;

    public CinemachineVirtualCamera backCamera;

    public bool canPlay = true;

    // public LayerMask targetLayer will be the layer on which target colliders are found.
    public LayerMask targetLayer;

    // public float checkRadius will be used to check collisions below the object using a spherecast.
    public float checkRadius;

    // public float frisbeeRadius will be used to check is the frisbee landed on the target.
    public float frisbeeRadius;

    public Vector3 angleOfThrow;

    public GameManager gm;

    public ParticleSystem respawnStart;
    public ParticleSystem respawnBurst;

    public Canvas UI;

    public int earnedPoints = 0;

    public int throwsRemaining = 1;

    public GameObject earnedPointsUI;

    //MARCO
    public ParticleSystem nukeParticle;
    public ParticleSystem multParticle;
    public ParticleSystem extraParticle;
    public ParticleSystem jackpotParticle;


    private AudioSource playerAudio;
    public AudioClip collectSound;
    public AudioClip nukeSound;
    public AudioClip throwSound;

    private AudioManager audioManager;

    public GameObject throwsRemainingUI;

    private int multiplier = 1;
    public bool shouldCheckForPoints = true;

    // Start is called before the first frame update
    void Start()
    {
        bc = this.GetComponent<BoxCollider>();
        rb = this.GetComponent<Rigidbody>();
        respawnStart = transform.GetChild(2).GetComponent<ParticleSystem>();
        respawnBurst = transform.GetChild(3).GetComponent<ParticleSystem>();
        nukeParticle = transform.GetChild(4).GetComponent<ParticleSystem>();
        multParticle = transform.GetChild(5).GetComponent<ParticleSystem>();
        extraParticle = transform.GetChild(5).GetComponent<ParticleSystem>();
        playerAudio = GetComponent<AudioSource>();
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        UI = GameObject.FindGameObjectWithTag("UI").GetComponent<Canvas>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        point1 = GameObject.Find("Point1").GetComponent<Transform>();
        point2 = GameObject.Find("Point2").GetComponent<Transform>();
        point3 = GameObject.Find("Point3").GetComponent<Transform>();
        point4 = GameObject.Find("Point4").GetComponent<Transform>();
        points = GameObject.Find("Points").GetComponent<Transform>();
        pivot = GameObject.Find("Pivot").GetComponent<Transform>();
        earnedPointsUI = GameObject.Find("EarnedPoints");
        throwsRemainingUI = GameObject.Find("Throws");
        throwFrisbee = this.GetComponent<FollowBezierCurve>().FollowPath();
        backCamera = GameObject.Find("BackCamera").GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canPlay)
        {
            // a and d are the keys which will be used to alter the direction the frisbee will be thrown.
            if (Input.GetKey(KeyCode.A))
            {
                points.Rotate(Vector3.down * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                points.Rotate(Vector3.up * speed * Time.deltaTime);
            }
            // w and s are the keys which will be used to determine the distance the frisbee will travel.
            if (Input.GetKey(KeyCode.W))
            {
                Vector3 position = point4.transform.position;
                Vector3 targetPosition = point1.transform.position;
                Vector3 direction = position - targetPosition;
                point2.transform.position += direction.normalized / 3 * speed * Time.deltaTime;
                point2.transform.position += new Vector3(0, 1, 0) / 3 * speed * Time.deltaTime;
                point3.transform.position += direction.normalized * 2 / 3 * speed * Time.deltaTime;
                point3.transform.position += new Vector3(0, 1, 0) * 2 / 3 * speed * Time.deltaTime;
                point4.transform.position += direction.normalized * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                Vector3 position = point4.transform.position;
                Vector3 targetPosition = point1.transform.position;
                Vector3 direction = position - targetPosition;
                point2.transform.position += direction.normalized / 3 * -1 * speed * Time.deltaTime;
                point2.transform.position -= new Vector3(0, 1, 0) / 3 * speed * Time.deltaTime;
                point3.transform.position += direction.normalized * 2 / 3 * -1 * speed * Time.deltaTime;
                point3.transform.position -= new Vector3(0, 1, 0) * 2 / 3 * speed * Time.deltaTime; 
                point4.transform.position += direction.normalized * -1 * speed * Time.deltaTime;

            }
            // q and e are the which will be used to determine the distance the frisbee will travel.
            if (Input.GetKey(KeyCode.Q))
            {
                float angle = transform.localEulerAngles.x;
                angle = (angle > 180) ? angle - 360 : angle;
                if (angle < 30)
                {
                    this.transform.Rotate(Vector3.right.normalized * speed * Time.deltaTime);
                    pivot.Rotate(Vector3.down * speed * Time.deltaTime);
                }
            }
            if (Input.GetKey(KeyCode.E))
            {
                float angle = transform.localEulerAngles.x;
                angle = (angle > 180) ? angle - 360 : angle;
                if (angle > -30)
                {
                    this.transform.Rotate(Vector3.left.normalized * speed * Time.deltaTime);
                    pivot.Rotate(Vector3.up * speed * Time.deltaTime);
                }
            }
            if (Input.GetKey(KeyCode.Space))
            {
                throwsRemaining -= 1;
                throwsRemainingUI.GetComponent<TextMeshProUGUI>().text = "Throws Left: " + throwsRemaining.ToString() + "/5";
                playerAudio.PlayOneShot(throwSound, audioManager.sfx);
                angleOfThrow = this.transform.eulerAngles;
                canPlay = false;
                StartCoroutine(throwFrisbee);
            }
        }
    }

    public void SetCanPlay(bool value)
    {
        if (value)
        {
            canPlay = true;
        }
        else
            canPlay = false;
    }

    public bool CheckIfLanded()
    {
        Collider[] collisions = Physics.OverlapSphere(this.transform.position, checkRadius, targetLayer);
        if (collisions.Length > 0)
        {
            return true;
        }
        else
            return false;
    }

    public int CheckPoints()
    {
        if (earnedPoints != 0)
            return 0;

        Collider[] collisions = Physics.OverlapSphere(this.transform.position, frisbeeRadius, targetLayer);
        if (collisions.Length > 0)
        {
            foreach (Collider collision in collisions)
            {
                switch (collision.gameObject.name)
                {
                    case "5":
                        {
                            if (5 > earnedPoints)
                            {
                                earnedPoints = 5;
                            }
                            break;
                        }
                    case "100":
                        {
                            if (100 > earnedPoints)
                            {
                                earnedPoints = 100;
                            }
                            break;
                        }
                    case "200":
                        {
                            if (200 > earnedPoints)
                            {
                                earnedPoints = 200;
                            }
                            break;
                        }
                    case "500":
                        {
                            if (500 > earnedPoints)
                            {
                                earnedPoints = 500;
                            }
                            break;
                        }
                    case "1000":
                        {
                            if (1000 > earnedPoints)
                            {
                                earnedPoints = 1000;
                            }
                            break;
                        }
                }
            }
        }
        shouldCheckForPoints = false;
        return earnedPoints;
    }

    private void OnCollisionEnter(Collision collision)
    {
        StopCoroutine(throwFrisbee);
        rb.constraints = RigidbodyConstraints.None;
        this.transform.GetChild(0).GetComponent<RotateObject>().rotateY = false;
        this.transform.GetChild(1).GetComponent<RotateObject>().rotateY = false;
        backCamera.enabled = false;
        if (collision.gameObject.CompareTag("Island"))
        {
            Debug.Log("COLLIDED WITH ISLAND");
            if(shouldCheckForPoints)
            {
                gm.score = CheckPoints() * multiplier;
                StartCoroutine(DisplayEarnedScore());
                gm.UpdateDisplayScore();
            }
        }
        if (collision.gameObject.CompareTag("SpinIsland"))
        {
            GameObject.FindWithTag("SpinIsland").transform.parent.GetComponent<RotateObject>().enabled = false;
        }
        else if (collision.gameObject.CompareTag("Nuke"))
        {
            playerAudio.PlayOneShot(nukeSound, audioManager.sfx);
            nukeParticle.Play();
            gm.powerup = "Nuke";
            StartCoroutine(gm.UpdatePowerUp());
            Destroy(collision.gameObject);
        }
        if (throwsRemaining > 0)
        {
            StartCoroutine(TimerResetPlayer(3));
        }
        else if (gm.levelNumber == gm.numOfLevels)
        {
            gm.UpdateHiScore();
            GameObject.Find("AllCanvas").transform.GetChild(4).gameObject.SetActive(true);
        }
        else
        {
            GameObject.Find("AllCanvas").transform.GetChild(3).gameObject.SetActive(true);
            GameObject.Find("AllCanvas").transform.GetChild(3).GetChild(0).gameObject.SetActive(true);
        }
    }

    public IEnumerator DisplayEarnedScore()
    {
        earnedPointsUI.GetComponent<TextMeshProUGUI>().text = "+" + gm.score;
        yield return new WaitForSeconds(2.0f);
        earnedPointsUI.GetComponent<TextMeshProUGUI>().text = "";
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Finish"))
        {
            StopCoroutine(throwFrisbee);
            rb.constraints = RigidbodyConstraints.None;

            if (throwsRemaining > 0)
            {
                StartCoroutine(TimerResetPlayer(3));
            }
        }
        else if (other.gameObject.CompareTag("Multiply"))
        {
            gm.powerup = "2x Score";
            StartCoroutine(gm.UpdatePowerUp());
            multiplier = 2;
            multParticle.Play();
            playerAudio.PlayOneShot(collectSound, audioManager.sfx);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("ExtraThrow"))
        {
            throwsRemaining += 1;
            throwsRemainingUI.GetComponent<TextMeshProUGUI>().text = "Throws Left: " + throwsRemaining.ToString() + "/5";
            extraParticle.Play();
            playerAudio.PlayOneShot(collectSound, audioManager.sfx);
            gm.powerup = "Extra Throw";
            StartCoroutine(gm.UpdatePowerUp());
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Jackpot"))
        {
            gm.score += 3000 * multiplier;
            StartCoroutine(DisplayEarnedScore());
            gm.UpdateDisplayScore();
            jackpotParticle.Play();
            playerAudio.PlayOneShot(collectSound, audioManager.sfx);
            gm.powerup = "Jackpot";
            StartCoroutine(gm.UpdatePowerUp());
            Destroy(other.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, checkRadius);
        Gizmos.DrawWireSphere(this.transform.position, frisbeeRadius);
    }

    public IEnumerator TimerResetPlayer(float time)
    {
        respawnStart.Play();
        yield return new WaitForSeconds(time - 0.5f);
        //display points earned
        respawnBurst.Play();
        yield return new WaitForSeconds(0.5f);
        ResetPlayer();
    }

    public void ResetPlayer()
    {
        earnedPoints = 0;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        GameObject.FindWithTag("SpinIsland").transform.parent.GetComponent<RotateObject>().enabled = true;
        this.GetComponent<PlayerController>().enabled = true;
        this.transform.position = new Vector3(0.0f, 1.0f, 0.0f);
        backCamera.enabled = true;
        canPlay = true;
        this.transform.GetChild(0).GetComponent<RotateObject>().rotateY = true;
        this.transform.GetChild(1).GetComponent<RotateObject>().rotateY = true;
        this.GetComponent<FollowBezierCurve>().timeChecker = 0.0f;
        this.transform.eulerAngles = angleOfThrow;
        UI.enabled = true;
        shouldCheckForPoints = true;
    }

}
