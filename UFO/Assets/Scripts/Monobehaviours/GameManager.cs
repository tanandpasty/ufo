using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int score = 0;
    public string powerup;
    public int levelNumber = 0;
    public int numOfLevels = 3;
    public int highScore = 0;
    public GameObject allCanvas;

    public TextMeshProUGUI displayScore;
    public TextMeshProUGUI displayHiScore;
    public TextMeshProUGUI displayPowerup;
    private AudioSource audioSource;

    private void Awake()
    {
        if (GameObject.FindGameObjectsWithTag("GameController").Length > 1)
        {
            Destroy(this.gameObject);
        }
        audioSource = GameObject.Find("AudioManager").GetComponent<AudioSource>();

    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().buildIndex > 0)
        {
            Debug.Log("Score found");
            displayScore = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();
            displayScore.text = "Score: " + highScore.ToString();
            displayPowerup = GameObject.Find("Powerup").GetComponent<TextMeshProUGUI>();
            audioSource.volume = .25f;

        }
        else
        {
            levelNumber = 0;
            audioSource.volume = .7f;
        }
    }

    public void UpdateDisplayScore()
    {
        if (displayScore != null)
        {
            highScore += score;
            displayScore.text = "Score: " + highScore.ToString();
        }
    }

    public IEnumerator UpdatePowerUp()
    {
        if (displayPowerup != null)
        {
            displayPowerup.text = powerup;
        }
        yield return new WaitForSeconds(3.0f);
        displayPowerup.text = "";
    }
    public void UpdateHiScore()
    {
        if (displayPowerup != null)
        {
            displayHiScore.text = "Congratulations! \n Your final score was: " + highScore.ToString();
        }
    }



    public void LoadNextLevel()
    {
        levelNumber += 1;
        if(levelNumber >= numOfLevels + 1)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            SceneManager.LoadScene(levelNumber);
        }
    }

    //quits the game
    public void QuitGame()
    {
        //Closes the application
        Application.Quit();
    }

}
