using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public Transform point1;
    public Transform point2;
    public Transform point3;
    public Transform point4;

    public LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
        point1 = GameObject.Find("Point1").transform;
        point3 = GameObject.Find("Point3").transform;
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, point1.localPosition);
        lineRenderer.SetPosition(1, point3.localPosition);
    }
}
