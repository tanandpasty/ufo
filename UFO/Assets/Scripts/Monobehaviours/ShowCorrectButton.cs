using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShowCorrectButton : MonoBehaviour
{
    public AudioManager audioManager;
    // Start is called before the first frame update

    private void Awake()
    {
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    void OnEnable()
    {
        if (this.gameObject.name == "MusicOn" && audioManager.musicOn)
        {
            this.gameObject.SetActive(false);
            Debug.Log(gameObject.name + "Has been set inactive");
        }
        else if (this.gameObject.name == "MusicOff" && !audioManager.musicOn)
        {
            this.gameObject.SetActive(false);
            Debug.Log(gameObject.name + "Has been set inactive");
        }
        else if (this.gameObject.name == "SFXOn" && audioManager.sfxOn)
        {
            this.gameObject.SetActive(false);
            Debug.Log(gameObject.name + "Has been set inactive");
        }
        else if (this.gameObject.name == "SFXOff" && !audioManager.sfxOn)
        {
            this.gameObject.SetActive(false);
            Debug.Log(gameObject.name + "Has been set inactive");
        }
    }
}
