using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBezierCurve : MonoBehaviour
{
    // public float speed will be used to determine how quickly this GameObject travels along the bezier curve.
    public float speed;

    // public Transform point1, point2, point3, and point4 will be used to determine the bezier curve which
    // this GameObject will follow.
    public Transform point1;
    public Transform point2;
    public Transform point3;
    public Transform point4;

    // public float timeChecker will be used to determine the location of this GameObject as it travels on
    // the bezier curve defined by point1, point2, point3, and point4.
    public float timeChecker;

    // Start is called before the first frame update
    void Start()
    {
        point1 = GameObject.Find("Point1").GetComponent<Transform>();
        point2 = GameObject.Find("Point2").GetComponent<Transform>();
        point3 = GameObject.Find("Point3").GetComponent<Transform>();
        point4 = GameObject.Find("Point4").GetComponent<Transform>();
        if(point1 != null && point2 != null && point3 != null && point4 != null)
        {
            Debug.Log("All points were found...");
        }
    }

    public IEnumerator FollowPath()
    {
        while (timeChecker < 1)
        {
            timeChecker += Time.deltaTime * speed;
            this.transform.position = Mathf.Pow(1 - timeChecker, 3) * point1.position +
                3 * Mathf.Pow(1 - timeChecker, 2) * timeChecker * point2.position +
                3 * (1 - timeChecker) * Mathf.Pow(timeChecker, 2) * point3.position +
                Mathf.Pow(timeChecker, 3) * point4.position;
            yield return new WaitForEndOfFrame();
        }
        timeChecker = 0.0f;
    }
}
